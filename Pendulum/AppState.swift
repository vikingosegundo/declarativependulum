//
//  AppState.swift
//  Pendulum
//
//  Created by Manuel Meyer on 31.08.22.
//

struct AppState:Codable {
    enum Change {
        case set(_Set)
        enum _Set {

            case first(Kind)
            case second(Kind)
            enum Kind {
                case simple(pendulum:SimplePendulum)
                case double(pendulum:DoublePendulum)
            }
        }
    }
    
    let simplePendulum0:SimplePendulum?
    let doublePendulum0:DoublePendulum?
    let simplePendulum1:SimplePendulum?
    let doublePendulum1:DoublePendulum?
    
    func alter(_ changes:  Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    func alter(_ changes: [Change]  ) -> Self { changes.reduce(self) { $0.alter($1) } }
    init() { self.init(nil,nil,nil,nil)}
    private init(
        _ s0:SimplePendulum?,
        _ s1:SimplePendulum?,
        _ d0:DoublePendulum?,
        _ d1:DoublePendulum?
    ) {
        simplePendulum0 = s0
        doublePendulum0 = d0
        simplePendulum1 = s1
        doublePendulum1 = d1
    }
    private func alter(_ cmd:Change) -> Self {
        switch cmd {
        case let .set(.first (.simple(pendulum:sp0))): return .init(sp0            , simplePendulum1,doublePendulum0, doublePendulum1)
        case let .set(.second(.simple(pendulum:sp1))): return .init(simplePendulum0, sp1            ,doublePendulum0, doublePendulum1)
        case let .set(.first (.double(pendulum:dp0))): return .init(simplePendulum0, simplePendulum1,dp0            , doublePendulum1)
        case let .set(.second(.double(pendulum:dp1))): return .init(simplePendulum0, simplePendulum1,doublePendulum0, dp1            )
        }
    }
    
}
