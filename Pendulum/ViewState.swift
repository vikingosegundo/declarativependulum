//
//  ViewState.swift
//  Pendulum
//
//  Created by Manuel Meyer on 31.08.22.
//

import SwiftUI

final class ViewState:ObservableObject {
    @Published var doublePendulum0:DoublePendulum?
    @Published var simplePendulum0:SimplePendulum?
    @Published var doublePendulum1:DoublePendulum?
    @Published var simplePendulum1:SimplePendulum?
    let roothandler:Input
    init(store:AppStore, roothandler r:@escaping Input) {
        roothandler = r
        store.updated {
            self.process(store:store)
        }
    }
    func handle(msg: Message) {
        DispatchQueue.main.async {
        }
    }
    private func process(store:AppStore) {
        simplePendulum0 = store.state().simplePendulum0
        doublePendulum0 = store.state().doublePendulum0
        simplePendulum1 = store.state().simplePendulum1
        doublePendulum1 = store.state().doublePendulum1
    }
}
