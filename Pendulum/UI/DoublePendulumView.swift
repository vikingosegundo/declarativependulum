//
//  DoublePendulumView.swift
//  Pendulum
//
//  Created by Manuel Meyer on 03.05.22.
//

import SwiftUI

struct DoublePendulumView: View {
    let display:PendulumNo; enum PendulumNo{ case first,second }
    
    @EnvironmentObject private var viewState: ViewState
    var pendulum: DoublePendulum? {
        display == .first ? viewState.doublePendulum0 : viewState.doublePendulum1
    }
    var body: some View {
        ZStack {
            if let pendulum = pendulum {
                Canvas { ctx, size in
                    let b0 = CGPoint(x: pendulum.bobPoints().0.0 + size.width / 2, y: pendulum.bobPoints().0.1 + size.height / 2)
                    let b1 = CGPoint(x: pendulum.bobPoints().1.0 + size.width / 2, y: pendulum.bobPoints().1.1 + size.height / 2)
                    ctx.stroke(Path(ellipseIn:CGRect(origin:b0,size:.init(width:pendulum.bobDiameter,height:pendulum.bobDiameter))),with:.color(.green), lineWidth:2)
                    ctx.stroke(Path(ellipseIn:CGRect(origin:b1,size:.init(width:pendulum.bobDiameter,height:pendulum.bobDiameter))),with:.color(.init(hue:pendulum.hue, saturation:0.5, brightness:1.0)), lineWidth:2)
                    ctx.fill(Path { path in
                        path.move   (to:CGPoint(x:size.width/2,y:size.height/2))
                        path.addLine(to:CGPoint(x:b0.x        ,y:b0.y         ))
                        path.addLine(to:CGPoint(x:b1.x        ,y:b1.y         ))
                        path.addLine(to:CGPoint(x:b0.x-1      ,y:b0.y-1       ))
                    }, with:.color(.black))
                }
                let p = (viewState.doublePendulum0,viewState.doublePendulum1)
                VStack {
                    if display == .second {
                        Spacer()
                        Button {
                                if p.0?.paused ?? false {
                                    viewState.roothandler(.start(.first (.double(.pendulum))))
                                    viewState.roothandler(.start(.second(.double(.pendulum))))
                                } else {
                                    viewState.roothandler(.stop(.first (.double(.pendulum))))
                                    viewState.roothandler(.stop(.second(.double(.pendulum))))
                                }
                        } label: {
                            if display == .second {
                                Text(p.0?.paused ?? false ? "play" : "pause")
                            } else {
                                EmptyView()
                            }
                        }
                    }
                }
            } else {
                EmptyView()
            }
        }
    }
}
