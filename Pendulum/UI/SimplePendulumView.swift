//
//  SimplePendulumView.swift
//  Pendulum
//
//  Created by Manuel Meyer on 18.05.22.
//

import SwiftUI

struct SimplePendulumView: View {
    let display:PendulumNo; enum PendulumNo{ case first,second }
    @EnvironmentObject private var viewState: ViewState
    var pendulum: SimplePendulum? {
        display == .first ? viewState.simplePendulum0 : viewState.simplePendulum1
    }
    var body: some View {
        ZStack {
            Canvas { ctx, size in
                if let pendulum = pendulum {
                    let b0 = CGPoint(x: pendulum.bobPoint().0 + size.width / 2, y: pendulum.bobPoint().1 + size.height / 2)
                    ctx.stroke(Path(ellipseIn: CGRect(origin:b0, size:.init(width:pendulum.bobDiameter, height:pendulum.bobDiameter))), with:.color(.init(hue:pendulum.hue, saturation:0.5, brightness:1.0)), lineWidth:2)
                    ctx.fill(Path { path in
                        path.move(to: CGPoint(x: size.width/2, y: size.height/2))
                        path.addLine(to: CGPoint(x: b0.x, y: b0.y))
                        path.addLine(to: CGPoint(x: b0.x-1, y: b0.y-1))
                    }, with:.color(.black))
                }
            }
            let p = (viewState.simplePendulum0,viewState.simplePendulum1)
            VStack {
                if display == .second {
                    Spacer()
                    Button {
                            if p.0?.paused ?? false {
                                viewState.roothandler(.start(.first(.simple(.pendulum))))
                                viewState.roothandler(.start(.second(.simple(.pendulum))))
                            } else {
                                viewState.roothandler(.stop(.first(.simple(.pendulum))))
                                viewState.roothandler(.stop(.second(.simple(.pendulum))))
                            }
                    } label: {
                        if display == .second {
                            Text(p.0?.paused ?? false ? "play" : "pause")
                        } else {
                            EmptyView()
                        }
                    }
                }
            }
        }
    }
}
