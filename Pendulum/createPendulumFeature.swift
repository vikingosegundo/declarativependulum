//
//  createPendulumFeature.swift
//  Pendulum
//
//  Created by Manuel Meyer on 31.08.22.
//

func createPendulumFeature(store:AppStore,out:Output) -> Input {
    return { msg in
        switch msg {
        case .start(.first (.simple(.pendulum))):store.change(.set(.first (.simple(pendulum:(store.state().simplePendulum0!.alter(.unpause))))))
        case .start(.second(.simple(.pendulum))):store.change(.set(.second(.simple(pendulum:(store.state().simplePendulum1!.alter(.unpause))))))
        case .start(.first (.double(.pendulum))):store.change(.set(.first (.double(pendulum:(store.state().doublePendulum0!.alter(.unpause))))))
        case .start(.second(.double(.pendulum))):store.change(.set(.second(.double(pendulum:(store.state().doublePendulum1!.alter(.unpause))))))
        case .stop (.first (.simple(.pendulum))):store.change(.set(.first (.simple(pendulum:(store.state().simplePendulum0!.alter(  .pause))))))
        case .stop (.second(.simple(.pendulum))):store.change(.set(.second(.simple(pendulum:(store.state().simplePendulum1!.alter(  .pause))))))
        case .stop (.first (.double(.pendulum))):store.change(.set(.first (.double(pendulum:(store.state().doublePendulum0!.alter(  .pause))))))
        case .stop (.second(.double(.pendulum))):store.change(.set(.second(.double(pendulum:(store.state().doublePendulum1!.alter(  .pause))))))
        case .tick:
            store.change(.set(.first (.simple(pendulum:(store.state().simplePendulum0!.alter(.tick))))))
            store.change(.set(.second(.simple(pendulum:(store.state().simplePendulum1!.alter(.tick))))))
            store.change(.set(.first (.double(pendulum:(store.state().doublePendulum0!.alter(.tick))))))
            store.change(.set(.second(.double(pendulum:(store.state().doublePendulum1!.alter(.tick))))))
        }
    }
}
