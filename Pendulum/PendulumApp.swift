//
//  PendulumApp.swift
//  Pendulum
//
//  Created by Manuel Meyer on 03.05.22.
//

import SwiftUI
import Foundation

fileprivate let store      : AppStore           = createDiskStore(pathInDocs: "pendulum.state.json")
fileprivate var viewState  : ViewState          = ViewState(store:store,roothandler:{ rootHandler($0) })
fileprivate var rootHandler: ((Message) -> ())! = createAppDomain(
    store      : store,
    receivers  : [viewState.handle(msg:)],
    rootHandler: { rootHandler($0) }
)
@main
struct PendulumApp: App {
    var body: some Scene {
        WindowGroup {
            VStack {
                ZStack {
                    SimplePendulumView(display:.first )
                    SimplePendulumView(display:.second)
                }
                ZStack {
                    DoublePendulumView(display:.first )
                    DoublePendulumView(display:.second)
                }
            }
            .environmentObject(viewState)
            .onReceive(timer) { _ in viewState.roothandler(.tick) }
            .onAppear(perform:{
                if store.state().doublePendulum0 == nil {
                    populate(store)
                }
            })
        }
    }
    private let timer = Timer.publish(every: 1.0/60.0, on: .main, in: .common).autoconnect()
}
private let baseAngle = (1.25 - 0.025 + .pi, 1.25 + .pi)
private let offset    = (0.05, 0.05)
private let masses    = ((25.0, 50.0),(25.0, 50.0))
private let radii     = (50.0, 50.0)
private let dampening = 0.0015
private let hues      = (0.0, 0.66) // red & blue

fileprivate func populate(_ store:AppStore) {
    DispatchQueue.main.asyncAfter(deadline: .now() ) {
        store.change(.set(.first(.simple(pendulum:SimplePendulum(
            angle: baseAngle.0,
            r: radii.0 + radii.1,
            gravity: 1,
            hue: hues.0
        ).alter(.pause)))))
        store.change(.set(.second(.simple(pendulum:SimplePendulum(
            angle: baseAngle.0 + offset.0,
            r: radii.0 + radii.1,
            gravity: 1 ,
            hue: hues.1
        ).alter(.pause)
        ))))
        store.change(.set(.first(.double(pendulum:DoublePendulum(
            radii: radii,
            masses: masses.0,
            angles: (baseAngle.0, baseAngle.1),
            dampening: dampening,
            hue: hues.0
        ).alter(.pause)))))
        store.change(.set(.second(.double(pendulum:DoublePendulum(
            radii: radii,
            masses: masses.1,
            angles: (baseAngle.0, baseAngle.1 + offset.1),
            dampening: dampening,
            hue: hues.1
        ).alter(.pause)
        ))))
    }
}
