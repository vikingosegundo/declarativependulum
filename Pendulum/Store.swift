//
//  StateHandler.swift
//  Todos
//
//  Created by Manuel Meyer on 12/04/2021.
//

typealias Access<S> = (                    ) -> S
typealias Change<C> = ( C...               ) -> ()
typealias Reset     = (                    ) -> ()
typealias Callback  = ( @escaping () -> () ) -> ()
typealias Destroy   = (                    ) -> ()

typealias Store<S,C> = (
      state: Access<S>,
     change: Change<C>,
      reset: Reset,
    updated: Callback,
    destroy: Destroy
)

func state  <S,C>(in store:Store<S,C>          ) -> S { store.state()               }
func change <S,C>(_  store:Store<S,C>,_ cs:C...)      { cs.forEach{store.change($0)}}
func reset  <S,C>(_  store:Store<S,C>          )      { store.reset()               }
func destroy<S,C>(_  store:inout Store<S,C>!   )      { store.destroy();store = nil }
