//
//  SimplePendulum.swift
//  Pendulum
//
//  Created by Manuel Meyer on 18.05.22.
//

import Foundation
struct SimplePendulum:Codable {
    enum Change {
        case tick
        case pause
        case unpause
        case set(_Set); enum _Set {
            case radius   (Double)
            case gravity  (Double)
            case dampening(Double)
        }
    }
    let angle      : Double
    let angleV     : Double
    let angleA     : Double
    let r          : Double
    let gravity    : Double
    let dampening  : Double
    let bobDiameter: Double
    let hue        : Double
    let paused     : Bool
    init(
            angle: Double,
                r: Double,
          gravity: Double,
      dampening d: Double = 0.0125,
   bobDiameter bd: Double = 6,
              hue: Double = 0.0,
           paused: Bool   = false
    ) {
        self.init(angle,0.0,0.0,r,gravity,d,bd,hue,paused)
    }
    func alter(_ changes:  Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    func alter(_ changes: [Change]  ) -> Self { changes.reduce(self) { $0.alter($1) } }
    func bobPoint() -> ((Double,Double)) { (r * sin(angle) ,r * cos(angle)) }
}

//MARK: - manipulate recursively & axiomatically
private
extension SimplePendulum {
    init(
     _       angle: Double,
     _      angleV: Double,
     _      angleA: Double,
     _           r: Double,
     _     gravity: Double,
     _   dampening: Double,
     _ bobDiameter: Double,
     _         hue: Double,
     _      paused: Bool
    ) {
        // math
        self.angle     = angle
        self.angleV    = angleV
        self.angleA    = angleA
        self.r         = r
        self.gravity   = gravity
        self.dampening = dampening
        //style & operations
        self.bobDiameter = bobDiameter
        self.hue         = hue
        self.paused      = paused
    }
    func alter(_ cmd:Change) -> Self {
        switch (paused,cmd) {
        case (false,       .tick        ): return advance()
        case ( true,    .unpause        ): return .init(angle,angleV,angleA,r,gravity,dampening,bobDiameter,hue,false )
        case (false,      .pause        ): return .init(angle,angleV,angleA,r,gravity,dampening,bobDiameter,hue,true  )
        case (_, let .set(.radius   (r))): return .init(angle,angleV,angleA,r,gravity,dampening,bobDiameter,hue,paused)
        case (_, let .set(.dampening(d))): return .init(angle,angleV,angleA,r,gravity,        d,bobDiameter,hue,paused)
        case (_, let .set(.gravity  (g))): return .init(angle,angleV,angleA,r,g      ,dampening,bobDiameter,hue,paused)
        case (_, _                      ): return self
        }
    }
}
// MARK: - // Calculations

private
extension SimplePendulum {
    func advance() -> Self {
        let force = gravity * sin(angle)
        let aa = (-1 * force) / r
        let av = (angleV + angleA) * (1.0 - dampening)
        let  a = angle + angleV
        return .init(a,av,aa,r,gravity,dampening,bobDiameter,hue,paused)
    }
}
