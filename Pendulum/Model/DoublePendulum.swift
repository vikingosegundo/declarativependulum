//
//  DoublePendulum.swift
//  Pendulum
//
//  Created by Manuel Meyer on 03.05.22.
//

import Foundation
struct DoublePendulum:Codable {
    enum Change {
        case tick
        case pause
        case unpause
        case set(_Set); enum _Set {
            case masses   ((Double,Double))
            case radii    ((Double,Double))
            case gravity  (Double)
            case dampening(Double)
        }
    }
    let bobDiameter: Double
    let hue        : Double
    let paused     : Bool
    init( radii r: (Double, Double),
         masses m: (Double, Double),
         angles a: (Double, Double),
      dampening d: Double = 0.0,
        gravity g: Double = 1.0,
   bobDiameter bd: Double = 6,
            hue h: Double = 0.0,
         paused p: Bool   = false
    ) {
        self.init(r,m,a,(0,0),d,g,bd,h,p)
    }
    func alter(_ changes:  Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    func alter(_ changes: [Change]  ) -> Self { changes.reduce(self) { $0.alter($1) } }
    func bobPoints() -> (
        (Double,Double),
        (Double,Double)
    ) {
        let p0 = (       r0 * sin(a0),        r0 * cos(a0))
        let p1 = (p0.0 + r1 * sin(a1), p0.1 + r1 * cos(a1))
        return (p0, p1)
    }
    private let r0 : Double
    private let r1 : Double
    private let m0 : Double
    private let m1 : Double
    private let a0 : Double
    private let a1 : Double
    private let av0: Double
    private let av1: Double
    private let g  : Double
    private let d  : Double
}

//MARK: - manipulate recursively & axiomatically
private
extension DoublePendulum {
    init(      _ r: (Double,Double),
               _ m: (Double,Double),
               _ a: (Double,Double),
              _ av: (Double,Double),
               _ d: Double,
               _ g: Double,
     _ bobDiameter: Double,
             _ hue: Double,
          _ paused: Bool
    ) {
        // math
        self.r0 = r.0
        self.r1 = r.1

        self.m0 = m.0
        self.m1 = m.1

        self.a0 = a.0
        self.a1 = a.1
        self.av0 = av.0
        self.av1 = av.1
        self.g = g
        self.d = d
        //style & operations
        self.bobDiameter = bobDiameter
        self.hue         = hue
        self.paused      = paused
    }
    func alter(_ cmd:Change) -> Self {
        switch (paused,cmd) {
        case (false,       .tick        ): return advance()
        case ( true,    .unpause        ): return .init((r0,r1),(m0,m1),(a0,a1),(av0,av1),d,g,bobDiameter,hue,false )
        case (false,      .pause        ): return .init((r0,r1),(m0,m1),(a0,a1),(av0,av1),d,g,bobDiameter,hue,true  )
        case (_, let .set(.radii    (r))): return .init(  r    ,(m0,m1),(a0,a1),(av0,av1),d,g,bobDiameter,hue,paused)
        case (_, let .set(.masses   (m))): return .init((r0,r1),  m    ,(a0,a1),(av0,av1),d,g,bobDiameter,hue,paused)
        case (_, let .set(.dampening(d))): return .init((r0,r1),(m0,m1),(a0,a1),(av0,av1),d,g,bobDiameter,hue,paused)
        case (_, let .set(.gravity  (g))): return .init((r0,r1),(m0,m1),(a0,a1),(av0,av1),d,g,bobDiameter,hue,paused)
        case (_, _                      ): return self
        }
    }
}
// MARK: - // Calculations
private
extension DoublePendulum {
    func advance() -> Self {
        let damp = (1.0 - d)
        let acc = calc()
        let av  = ((av0 + acc.0) * damp, (av1 + acc.1) * damp)
        let a   = ((a0 + av0), (a1 + av1))
        return .init((r0,r1),(m0,m1),a,av,d,g,bobDiameter,hue,paused)
    }
    func calc() -> (Double, Double) { // -> (pendulum arm 0, pendulum arm 1)
        //       |<--------------- pendulum arm 0 ----------->|<-------- pendulum arm 1 ------>|
        let n0 = (-g * (2 * m0 + m1) * sin(a0),                2 * sin(a0 - a1)                )
        let n1 = (-m1 * g * sin(a0 - 2 * a1),                  av0 * av0 * r0 * (m0 + m1)      )
        let n2 = (-2 * sin(a0 - a1) * m1,                      g * (m0 + m1) * cos(a0)         )
        let n3 = (av1 * av1 * r1 + av0 * av0 * r0 * cos(a0-a1),av1 * av1 * r1 * m1 * cos(a0-a1))
        
        let d = (r0 * (2 * m0 + m1 - m1 * cos(2*a0-2*a1)),
                 r1 * (2 * m0 + m1 - m1 * cos(2*a0-2*a1)))
        return (
            (n0.0 +  n1.0 + (n2.0 * n3.0) / d.0).truncatingRemainder(dividingBy:.pi * 2) / 360.0,
            (n0.1 * (n1.1 +  n2.1 + n3.1) / d.1))
    }
}
