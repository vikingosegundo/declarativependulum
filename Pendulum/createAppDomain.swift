//
//  createAppDomain.swift
//  Pendulum
//
//  Created by Manuel Meyer on 31.08.22.
//

enum Message {
    case start(Start)
    case stop (Stop )
    case tick
    
    enum Start {
        case first (Kind)
        case second(Kind)
    }
    enum Stop {
        case first (Kind)
        case second(Kind)
    }
    enum Kind {
        case simple(Pendulum)
        case double(Pendulum)
        enum Pendulum {
            case pendulum
        }
    }
}

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()

typealias AppStore = Store<AppState,AppState.Change>

func createAppDomain(
    store      : AppStore,
    receivers  : [Input],
    rootHandler: @escaping Output) -> Input
{
    let features: [Input] = [
        createPendulumFeature(store:store,out:rootHandler)
    ]
    return { msg in
        (receivers + features).forEach { $0(msg) }
    }
}
